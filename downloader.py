from os import makedirs
from random import choice
from re import search

from requests import Session

SAVE_FOLDER = "download_audios"
LOGIN = ""
PASSWORD = ""
BASE_API_URL = "https://api.vk.com/method/"

makedirs(SAVE_FOLDER, exist_ok=True)

request = Session()
request.headers['User-agent'] = "VKAndroidApp/4.38-849 (Android 6.0; SDK 23; x86; Google Nexus 5X; ru)"

token = request.post(f"https://oauth.vk.com/token",
                     data={'grant_type': 'password', 'client_id': 2274003, 'client_secret': 'hHbZxrka2uZ6jB1inYsH',
                           'username': LOGIN, 'password': PASSWORD, '2fa_supported': 1, 'lang': 'ru',
                           'device_id': ''.join([choice('0123456789abcdef') for _ in range(16)]),
                           'scope': 'all'}).json()['access_token']


def execute_method(method: str, **kwargs):
    a = request.post(
        BASE_API_URL + method,
        data={'access_token': token, 'v': '5.100', **kwargs}
    ).json()

    return a


uid = execute_method('users.get')['response'][0]['id']


def convert_to_mp3(url):
    out = search(r"(https://.+/).+/(.+)/", url)
    return out.group(1) + out.group(2) + '.mp3'


audios = execute_method('audio.get', owner_id=uid)["response"]["items"]
print(f"Всего аудио: {len(audios)}")

for k, item in enumerate(audios, 1):
    name = item["artist"] + " - " + item["title"]

    with open(SAVE_FOLDER + '/' + name + '.mp3', 'wb') as f:
        f.write(request.post(convert_to_mp3(item["url"])).content)

    print(f"{k}) DOWNLOADED: {name}")
